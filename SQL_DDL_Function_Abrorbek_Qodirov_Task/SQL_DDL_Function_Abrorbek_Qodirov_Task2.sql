CREATE OR REPLACE FUNCTION get_sales_revenue_by_category_qtr(current_qtr INT)
RETURNS TABLE (category VARCHAR, total_sales_revenue DECIMAL) AS
$$
BEGIN
    RETURN QUERY
    SELECT 
        c.name AS category,
        SUM(p.amount) AS total_sales_revenue
    FROM 
        payment p
    JOIN 
        rental r ON p.rental_id = r.rental_id
    JOIN 
        inventory i ON r.inventory_id = i.inventory_id
    JOIN 
        film f ON i.film_id = f.film_id
    JOIN 
        film_category fc ON f.film_id = fc.film_id
    JOIN 
        category c ON fc.category_id = c.category_id
    WHERE 
        p.payment_date >= DATE_TRUNC('quarter', CURRENT_DATE)
        AND EXTRACT(QUARTER FROM p.payment_date) = current_qtr
    GROUP BY 
        c.name
    HAVING 
        SUM(p.amount) > 0;
END;
$$ LANGUAGE plpgsql;
