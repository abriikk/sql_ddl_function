CREATE OR REPLACE FUNCTION new_movie(movie_title VARCHAR)
RETURNS VOID AS
$$
DECLARE
    new_film_id INT;
    language_id INT;
BEGIN
    -- Get language_id for "Klingon" language
    SELECT language_id INTO language_id
    FROM language
    WHERE name = 'Klingon';

    IF NOT FOUND THEN
        RAISE EXCEPTION 'Language not found';
    END IF;

    -- Generate unique film_id
    SELECT MAX(film_id) + 1 INTO new_film_id
    FROM film;

    -- Insert new movie into film table
    INSERT INTO film (film_id, title, rental_rate, rental_duration, replacement_cost, release_year, language_id)
    VALUES (new_film_id, movie_title, 4.99, 3, 19.99, EXTRACT(YEAR FROM CURRENT_DATE), language_id);

    IF NOT FOUND THEN
        RAISE EXCEPTION 'Error inserting new movie';
    END IF;
END;
$$ LANGUAGE plpgsql;
